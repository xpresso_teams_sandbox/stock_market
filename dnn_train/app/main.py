"""
This is the implementation of data preparation for sklearn
"""

import warnings
warnings.filterwarnings('ignore')
import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

import numpy as np
import pandas as pd
from statsmodels.tsa.arima_model import ARIMA
from fbprophet import Prophet
import os
import pickle

__author__ = "suvom shaw"

logger = XprLogger("dnn_train",level=logging.INFO)


class DnnTrain(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self):
        super().__init__(name="DnnTrain")
        """ Initialize all the required constansts and data her """

        self.train_path='/data/train.csv'
        self.test_path='/data/test.csv'
        logger.info("Data read start")
        self.train=pd.read_csv(self.train_path)
        self.test=pd.read_csv(self.test_path)
        logger.info("Data read End")
        self.arima=None
        self.p=2
        self.d=2
        self.q=1
        self.prophet=None
        self.arima=None
        self.arima_path='arima.pkl'
        self.prophet_path='prophet.pkl'


    @staticmethod
    def mean_absolute_percentage_error(y_true, y_pred): 
        logger.info("mape calculation")
        y_true, y_pred = np.array(y_true), np.array(y_pred)
        return np.mean(np.abs((y_true - y_pred) / y_true)) * 100

    @staticmethod
    def smape(A, F):
        logger.info("smape calculation")
        return 100/len(A) * np.sum(2 * np.abs(F - A) / (np.abs(A) + np.abs(F)))

    @staticmethod
    def model_arima(train,test,p,d,q):
        logger.info("Arima start")
        train=train[['close']]
        test=test[['close']]
        model = ARIMA(train, order=(p, d, q))  
        self.arima = model.fit(disp=-1)
        #pickle.dump(self.arima, open(self.arima_path, 'wb'))
        fc, se, conf = self.arima.forecast(test.shape[0], alpha=0.05)  # 95% conf
        # Make as pandas series
        fc_series = pd.Series(fc, index=test.index)
        logger.info("Arima  End")
        mape=DnnTrain.mean_absolute_percentage_error(test.close,fc_series)
        smape=DnnTrain.smape(test.close,fc_series)

        return mape,smape

    @staticmethod
    def model_prophet(train,test):
        logger.info("prophet start")

        train['date_time'] = pd.to_datetime(train['date_time'],format='%Y-%m-%d')
        test['date_time'] = pd.to_datetime(test['date_time'],format='%Y-%m-%d')

        train.rename(columns={"date_time": "ds", "close": "y"},inplace=True)
        test.rename(columns={"date_time": "ds", "close": "y"},inplace=True)
        train=train[['ds','y']]
        
        prophet=Prophet(growth='linear')
        self.prophet.fit(train)
        #pickle.dump(self.prophet, open(self.prophet_path, 'wb'))
        PredictionData = self.prophet.make_future_dataframe(periods=test.shape[0])
        predict =self. prophet.predict(PredictionData)
        logger.info("prophet End")
        mape=DnnTrain.mean_absolute_percentage_error(test.y,predict['yhat'][train.shape[0]:])
        smape=DnnTrain.smape(test.y,predict['yhat'][train.shape[0]:])
        return mape,smape






    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            logger.info("Armia Model start")
            mape_arima,smape_arima=DnnTrain.model_arima(self.train,self.test,self.p,self.d,self.q)
            self.send_metrics("Mape score of Arima model ", mape_arima)
            self.send_metrics("Smape score of Arima model ", smape_arima)
            result="Mape: "+str(mape_arima)+" smape: "+str(smape_arima)
            logger.info(result)
            logger.info("Armia Model End")

            logger.info("Prophet Model start")
            mape_prophet,smape_prophet=DnnTrain.model_prophet(self.train,self.test)
            self.send_metrics("Mape score of Prophet model ", mape_prophet)
            self.send_metrics("Smape score of Prophet model ", smape_prophet)
            result="Mape: "+str(mape_prophet)+" smape: "+str(smape_prophet)
            logger.info(result)
            logger.info("Prophet Model End")



        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self,desc,value):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "desc"},
                "metric": {"metric_key": value}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=True, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:

            if not os.path.exists(self.OUTPUT_DIR):
                os.makedirs(self.OUTPUT_DIR)
            self.test.to_csv(os.path.join(self.OUTPUT_DIR,"test.csv"),index=False)
            self.train.to_csv(os.path.join(self.OUTPUT_DIR, "train.csv"), index=False)
            pickle.dump(self.prophet, open(os.path.join(self.OUTPUT_DIR,self.prophet_path), 'wb'))
            pickle.dump(self.arima, open(os.path.join(self.OUTPUT_DIR,self.arima_path), 'wb'))
            logging.info("Data saved")

            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    
    data_prep = DnnTrain()
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
