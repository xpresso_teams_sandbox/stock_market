"""
This is the implementation of data preparation for sklearn
"""


import warnings
warnings.filterwarnings('ignore')

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger

import pandas as pd
import os

__author__ = "suvom shaw"

logger = XprLogger("dnn_data_prep",level=logging.INFO)


class DnnDataPrep(AbstractPipelineComponent):
    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.


    """

    def __init__(self,mount_path,filename):
        super().__init__(name="DnnDataPrep")
        """ Initialize all the required constansts and data her """
        file_path=os.path.join(mount_path,filename)
        logger.info("read data")
        self.data=pd.read_csv(file_path)
        logger.info("read complete")
        self.train=None
        self.test=None
        self.train_path='/data/train.csv'
        self.test_path='/data/test.csv'

    @staticmethod
    def convert_data_type(df):
        logger.info("convert data start")
        convert_dict = {'close': float}        
        df = df.astype(convert_dict)
        df['date_time'] = pd.to_datetime(df['date_time'],format='%Y-%m-%d')
        logger.info("convert data End")
        return df

    @staticmethod
    def train_test_split(df):
        logger.info("train test split start")
        length=df.shape[0]
        train=df[:int(length*0.8)]
        test=df[int(length*0.8):]
        logger.info("train test split End")
        return train,test




    def start(self, run_name):
        """
        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        try:
            super().start(xpresso_run_name=run_name)
            # === Your start code base goes here ===
            logger.info("Data Prep start")
            preprossed_data=DnnDataPrep.convert_data_type(self.data)
            print(preprossed_data.shape)
            self.train,self.test=DnnDataPrep.train_test_split(preprossed_data)
            self.train.to_csv(self.train_path,index=False)
            self.send_metrics("Shape of the train data", self.train)
            self.send_metrics("Shape of the test data", self.test)
            self.test.to_csv(self.test_path,index=False)
            logger.info("Data Prep End")

        except Exception:
            import traceback
            traceback.print_exc()
            self.completed(success=False)
        self.completed()

    def send_metrics(self,desc,data):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"rows": data.shape[0],
                            "column":data.shape[1] }
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        try:
            super().completed(push_exp=push_exp)
        except Exception:
            import traceback
            traceback.print_exc()

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        try:
            super().terminate()
        except Exception:
            import traceback
            traceback.print_exc()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        try:
            super().pause()
        except Exception:
            import traceback
            traceback.print_exc()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        try:
            super().restart()
        except Exception:
            import traceback
            traceback.print_exc()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py
    
    mount_path='/data'
    filename='stock_data.csv'
    data_prep = DnnDataPrep(mount_path,filename)
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
