"""
This is a sample hello world app
It prints hello world 100 times
"""
__author__ = "suvom shaw"

import warnings
warnings.filterwarnings('ignore')
from pyhive import hive
import pandas as pd

import logging
from xpresso.ai.core.logging.xpr_log import XprLogger
logger = XprLogger("data_fetch",level=logging.INFO)

def read_query(query):
    host = "172.16.6.81"
    port = 10000
    database = 'suvom'
    conn=hive.Connection(host=host, port=port, database=database)
    result=pd.read_sql(query,conn)
    conn.close()
    return result

def covert_data_type(df):
    convert_dict = {'close': float} 
    df = df.astype(convert_dict)
    df['date_time'] = pd.to_datetime(df['date_time'],format='%Y-%m-%d')
    return df
    
if __name__ == '__main__':

    logger.info(str("Data fetch complete"))

    query="select close,date_time from historical_stock_prices where ticker= 'AHH'" 
    df=read_query(query)
    df=covert_data_type(df)
    logger.info(str(df.shape[0]))

    df.to_csv('/data/stock_data.csv',index=False)

    logger.info(str("Data fetch complete"))



  
